# PDP-11

these are my nostalgi files

NB: it's PDP-11, it has no passwords and sensitive info; for OpenVMS/VAX see private repo

# notes

* http://decuser.blogspot.com/2015/12/tutorial-setting-up-rt-11-v53-on-simh.html
* apt-get install simh
* pdp11 <filename.ini>
  On Arch linux, here in this directory, run 'simh-pdp11 boot.ini'
* `boot.ini` uses two disk images, working and storage, as `rl0` and `rl1` devices
* for convenience I have created `startme.sh` with 80x24 console (for KED to work)
* to exit, Ctrl-E (exit from OS to emulator), and then `quit` to quit emulator.

