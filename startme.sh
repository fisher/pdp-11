#!/bin/sh

# Input Mono should be installed; otherwise, just use the second line with defaults

xterm -bc -uc -bcn 250 -fg green -bg black -cr cyan -fa InputMono\ Light -fs 14 -title "PDP-11 term" -geometry 80x24 -e 'simh-pdp11 boot.ini'
#xterm -bc -bcn 250 -fg green -bg black -cr cyan -title "PDP-11 term" -geometry 80x24 -e 'simh-pdp11 boot.ini'

